package org.kramerlab.wekarest.factories;

import org.kramerlab.wekarest.cluster.ClusterService;
import org.kramerlab.wekarest.impl.ClusterImpl;

public class ClusterFactory  {
    private final static ClusterService service = new ClusterImpl();
    public static ClusterService getCluster() {
        return service;
    }
}
