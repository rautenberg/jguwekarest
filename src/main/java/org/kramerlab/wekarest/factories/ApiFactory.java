package org.kramerlab.wekarest.factories;

import org.kramerlab.wekarest.ApiService;
import org.kramerlab.wekarest.impl.ApiImpl;


public class ApiFactory {
    private final static ApiService service = new ApiImpl();
    public static ApiService getApiApi() {
        return service;
    }
}
