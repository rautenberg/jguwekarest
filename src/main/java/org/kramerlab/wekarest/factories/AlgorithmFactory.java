package org.kramerlab.wekarest.factories;

import org.kramerlab.wekarest.impl.AlgorithmImpl;
import org.kramerlab.wekarest.AlgorithmService;

public class AlgorithmFactory {
    private final static AlgorithmService service = new AlgorithmImpl();
    public static AlgorithmService getAlgorithm() {
        return service;
    }
}
